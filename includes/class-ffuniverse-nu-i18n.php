<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://guide.ffuniverse.nu/
 * @since      1.0.0
 *
 * @package    Ffuniverse_Nu
 * @subpackage Ffuniverse_Nu/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Ffuniverse_Nu
 * @subpackage Ffuniverse_Nu/includes
 * @author     Bobby Westberg <bellfalasch@gmail.com>
 */
class Ffuniverse_Nu_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'ffuniverse-nu',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
