<?php

/**
 * Fired during plugin activation
 *
 * @link       https://guide.ffuniverse.nu/
 * @since      1.0.0
 *
 * @package    Ffuniverse_Nu
 * @subpackage Ffuniverse_Nu/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Ffuniverse_Nu
 * @subpackage Ffuniverse_Nu/includes
 * @author     Bobby Westberg <bellfalasch@gmail.com>
 */
class Ffuniverse_Nu_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		// Silence is golden
	}
}