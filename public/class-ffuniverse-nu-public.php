<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://guide.ffuniverse.nu/
 * @since      1.0.0
 *
 * @package    Ffuniverse_Nu
 * @subpackage Ffuniverse_Nu/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Ffuniverse_Nu
 * @subpackage Ffuniverse_Nu/public
 * @author     Bobby Westberg <bellfalasch@gmail.com>
 */
class Ffuniverse_Nu_Public {

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $plugin_name       The name of the plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct( $plugin_name, $version ) {

        $this->plugin_name = $plugin_name;
        $this->version = $version;
        $this->wp_cbf_options = get_option($this->plugin_name);

        // Default settings:
        $this->topnav_version = 5; // TopNav5 (option 1 in admin).
        $this->endnav_version = 2; // Full (guide-variant)

        // TopNav - Respect setting from admin page.
        if (!empty($this->wp_cbf_options['tn5'])) {
          if ($this->wp_cbf_options['tn5'] == "2") {
            $this->topnav_version = 3;
          }
          if ($this->wp_cbf_options['tn5'] == "1") {
            $this->topnav_version = 5;
          }
        }

        if (!empty($this->wp_cbf_options['endnav'])) {
          if ($this->wp_cbf_options['endnav'] == "1") {
            $this->endnav_version = 1;
          }
          if ($this->wp_cbf_options['endnav'] == "2") {
            $this->endnav_version = 2;
          }
        }

    }

    /**
     * Register the stylesheets for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_styles() {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Ffuniverse_Nu_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Ffuniverse_Nu_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        if ($this->topnav_version === 5) {
          wp_enqueue_style( $this->plugin_name . "-topnav-modern", plugin_dir_url( __FILE__ ) . 'css/topnav5.css', array(), $this->version, 'all' );
          wp_enqueue_style( $this->plugin_name . "-topnav-modern-g2top", plugin_dir_url( __FILE__ ) . 'css/go-to-top.css', array(), $this->version, 'all' );
        } elseif ($this->topnav_version === 3) {
          wp_enqueue_style( $this->plugin_name . "-topnav-legacy", plugin_dir_url( __FILE__ ) . 'css/topnav3.css', array(), $this->version, 'all' );
        }

        if ($this->endnav_version === 1) {
          wp_enqueue_style( $this->plugin_name . "-endnav-light", plugin_dir_url( __FILE__ ) . 'css/endnav-light.css', array(), $this->version, 'all' );
        } elseif ($this->endnav_version === 2) {
          wp_enqueue_style( $this->plugin_name . "-endnav-guide", plugin_dir_url( __FILE__ ) . 'css/endnav-guide.css', array(), $this->version, 'all' );
        }

    }

    /**
     * Register the JavaScript for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts() {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Ffuniverse_Nu_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Ffuniverse_Nu_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        if ($this->topnav_version === 5) {
            wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/topnav5.js', array(), $this->version, false );
        } elseif ($this->topnav_version === 3) {
            wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/topnav3.js', array(), $this->version, false );
        }

    }

    public function display_topnav() {
      if($this->topnav_version === 5){
        include_once( 'partials/topnav5.php' );
        include_once( 'partials/go-to-top.php' );
      } elseif ($this->topnav_version === 3) {
        include_once( 'partials/topnav3.php' );
      }
    }

    public function display_endnav() {
      if ($this->endnav_version === 1) {
        include_once( 'partials/endnav-light.php' );
      } elseif ($this->endnav_version === 2) {
        include_once( 'partials/endnav-guide.php' );
      }
    }


    private function disable_emojicons_tinymce( $plugins ) {
      if ( is_array( $plugins ) ) {
        return array_diff( $plugins, array( 'wpemoji' ) );
      } else {
        return array();
      }
    }

   /**
    * Insert all FFU specific site-wide RSS feeds into head of all FFU-sites using this plugin.
    *
    * @since    1.6.0
    */
   public function wp_ffu_rssfeeds() {
     ?>
     <link rel="alternate" type="application/rss+xml" title="Final Fantasy Nyheter &raquo; Feed" href="https://nyheter.ffuniverse.nu/feed/" />
     <link rel="alternate" type="application/rss+xml" title="FFUniverse, uppdateringar &raquo; Feed" href="https://guide.ffuniverse.nu/rss-uppdateringar/" />
     <link rel="alternate" type="application/rss+xml" title="Utvecklarbloggen på FFUniverse &raquo; Feed" href="https://dev.ffuniverse.nu/feed/" />
     <?php
   }

    /**
    * Cleanup functions depending on each checkbox returned value in admin
    *
    * @since    1.0.0
    */
   // Cleanup head
   public function wp_cbf_cleanup() {

       if($this->wp_cbf_options['cleanup']){

           remove_action( 'wp_head', 'rsd_link' );                 // RSD link
           remove_action( 'wp_head', 'feed_links_extra', 3 );            // Category feed link
           remove_action( 'wp_head', 'feed_links', 2 );                // Post and comment feed links
           remove_action( 'wp_head', 'index_rel_link' );
           remove_action( 'wp_head', 'wlwmanifest_link' );
           remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );        // Parent rel link
           remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );       // Start post rel link
           //remove_action( 'wp_head', 'rel_canonical', 10, 0 );
           remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 );
           remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 ); // Adjacent post rel link
           remove_action( 'wp_head', 'wp_generator' );               // WP Version

            // all actions related to emojis
            remove_action( 'admin_print_styles', 'print_emoji_styles' );
            remove_action( 'wp_print_styles', 'print_emoji_styles' );
            remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
            remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
            remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
            remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
            remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
            // filter to remove TinyMCE emojis
            //add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );

            // Deactivate Rest API and meta
            remove_action( 'wp_head',      'rest_output_link_wp_head'              );
            remove_action( 'wp_head',      'wp_oembed_add_discovery_links'         );
            remove_action( 'template_redirect', 'rest_output_link_header', 11 );
            
            // Remove the REST API endpoint.
            remove_action( 'rest_api_init', 'wp_oembed_register_route' );

            // Turn off oEmbed auto discovery.
            add_filter( 'embed_oembed_discover', '__return_false' );

            // Don't filter oEmbed results.
            remove_filter( 'oembed_dataparse', 'wp_filter_oembed_result', 10 );

            // Remove oEmbed discovery links.
            remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );

            // Remove oEmbed-specific JavaScript from the front-end and back-end.
            remove_action( 'wp_head', 'wp_oembed_add_host_js' );

       }
   }

   // Cleanup head
   public function wp_cbf_remove_x_pingback($headers) {
       if(!empty($this->wp_cbf_options['cleanup'])){
           unset($headers['X-Pingback']);
           return $headers;
       }
   }

   // Remove Comment inline CSS
   public function wp_cbf_remove_comments_inline_styles() {
       if(!empty($this->wp_cbf_options['comments_css_cleanup'])){
           global $wp_widget_factory;
           if ( has_filter( 'wp_head', 'wp_widget_recent_comments_style' ) ) {
               remove_filter( 'wp_head', 'wp_widget_recent_comments_style' );
           }

           if ( isset($wp_widget_factory->widgets['WP_Widget_Recent_Comments']) ) {
               remove_action( 'wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style') );
           }
       }
   }

   // Remove gallery inline CSS
   public function wp_cbf_remove_gallery_styles($css) {
       if(!empty($this->wp_cbf_options['gallery_css_cleanup'])){
           return preg_replace( "!<style type='text/css'>(.*?)</style>!s", '', $css );
       }

   }

    public function hide_admin_bar_from_front_end(){
      if (is_blog_admin()) {
         return true;
      }
      remove_action( 'wp_head', '_admin_bar_bump_cb' );
      return false;
    }

}
