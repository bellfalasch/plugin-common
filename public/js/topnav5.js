// DOM READY
// Mozilla, Opera, Webkit
if ( document.addEventListener ) {
  document.addEventListener( "DOMContentLoaded", function(){
    document.removeEventListener( "DOMContentLoaded", arguments.callee, false);
    domReady();
  }, false );

// If IE event model is used
} else if ( document.attachEvent ) {
  // ensure firing before onload
  document.attachEvent("onreadystatechange", function(){
    if ( document.readyState === "complete" ) {
      document.detachEvent( "onreadystatechange", arguments.callee );
      domReady();
    }
  });
}

// CONSTANTS
var topnavClass = '.ffu_tn5_wrapper';
var topnavTogglerClass = '.ffu_tn5_menu_toggler';
var topnavSiteNavClass = '.ffu_tn5_sites_block .sites';
var topnavGuideNavClass = '.ffu_tn5_games_block';
var topnavLogoClass = '.ffu_tn5_logo';
var menuState = "menu-open";


// Let's go!
function domReady() {
	var topnav = getTopnav();
	if (topnav) {
		fetchTopNavJson(topnav);
		clickEvents(topnav);
		//getScrollTop();
		//moveSiteNavIntoGuideNav(topnav);
    greedyNavOrganizer(topnav);
    resizeListener(topnav);
	}
};

function resizeListener(topnav) {
  var reorganizeElements = debounce( function() {
    greedyNavOrganizer(topnav);
  }, 250);
  window.addEventListener('resize', reorganizeElements);
}

function greedyNavOrganizer(topnav) {
	var minW = 550; // Where mobile navigation kicks in
	var maxW = 1280; // Where we have full desktop experience

  // Check screen width. <1280 and >550 is when it should be moved. Otherwise moved back.
  // https://stackoverflow.com/questions/1248081/get-the-browser-viewport-dimensions-with-javascript
  var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
  // Get the menu for spinoffs and move it if needed.
  var spinoffMenu = topnav.querySelector('.ffu_tn5_guides_spinoffs');
  //var spinoffs = spinoffMenu.querySelectorAll('.topnav-games-item');
  // Insert them first here: .ffu_tn5_guides_other-games .topnav-game-dropdown
  var dropdown = topnav.querySelector('.ffu_tn5_guides_other-games .topnav-game-dropdown');

  if (w >= minW && w < maxW) {
    dropdown.insertAdjacentElement('afterbegin', spinoffMenu);
  } else {
    var originalLocation = topnav.querySelector('.ffu_tn5_guides_main-series + .menu-label');
    originalLocation.insertAdjacentElement('afterend', spinoffMenu);
  }
  // If we resize screen and leave mobile, check if menu is open - if so, close it and move menu back.
  if (w >= minW) {
	  var body = document.querySelector("body");
	  if (body.classList.contains(menuState)) {
     	closeMenu(body,topnav);
	  }
  }
}


// Safely return (if found) the main Topnav element.
function getTopnav() {
	var topnav = document.querySelector(topnavClass);
//	if (topnav.length) { topnav = topnav[0]; }
	if (topnav) { return topnav } else { return false };
}

var timeoutID = undefined; // Store timeout we need for moving an element within the menu at a specific time.

// When menu is opened (only used on mobile) lets move site nav into guide nav to make menu slidein better (less painful to style).
function moveSiteNavIntoGuideNav(topnav) {
//	if (topnav.classList.contains("menu-open")) {
		var sites = topnav.querySelector(topnavSiteNavClass);
		var guides = topnav.querySelector(topnavGuideNavClass);
		guides.insertAdjacentElement('afterbegin', sites);
		window.clearTimeout(timeoutID); // Kill the timer for "move after menu closed".
//	}
}
// When menu is closed (not on init but after being manually opened first) move site nav back to where it was.
function moveSiteNavBack(topnav) {
//	if (topnav.classList.contains("menu-open")) {
	  var body = document.querySelector("body");
		var sites = topnav.querySelector(".ffu_tn5_games_block .sites"); // Menu is relocated, so use new selector.
		var logo = topnav.querySelector(topnavLogoClass);

		timeoutID = window.setTimeout(function(){
      logo.insertAdjacentElement('afterend', sites);
      //topnav.classList.remove(menuState);
      //body.classList.remove(menuState);
    }, 400);
//	}
}

// Note to self: this will not be implemented until at a later stage, don't over complicate this!
// Note 2: adding links to possible ways to add extra smartness to the menu later.

// Detect if scrolled to the top, add class then, otherwise remove.
// http://www.softcomplex.com/docs/get_window_size_and_scrollbar_position.html
// On load run this, then on scroll run too - https://stackoverflow.com/questions/27043273/jquery-toggle-header-class-on-scroll-and-if-page-is-not-at-top
// Todo: add debounce - http://benalman.com/projects/jquery-throttle-debounce-plugin/
// Todo: detect if in view - https://stackoverflow.com/questions/487073/check-if-element-is-visible-after-scrolling
// Detect scrolling upwards - https://stackoverflow.com/questions/31223341/detecting-scroll-direction
/*
function getScrollTop() {
	var scrollTop;
	if(typeof(window.pageYOffset) == 'number')
	{
		// DOM compliant, IE9+
		scrollTop = window.pageYOffset;
	}
	else
	{
		// IE6-8 workaround
		if(document.body && document.body.scrollTop)
		{
			// IE quirks mode
			scrollTop = document.body.scrollTop;
		}
		else if(document.documentElement && document.documentElement.scrollTop)
		{
			// IE6+ standards compliant mode
			scrollTop = document.documentElement.scrollTop;
		}
	}
	return scrollTop;
}
*/

// Important click-event handling
function clickEvents(topnav) {
	var body = document.querySelector("body");

	// Let click toggle menu on and off
	topnav.querySelector(topnavTogglerClass).addEventListener('click', function() {

		// Add or remove "menu-open" state.
		if (topnav.classList.contains(menuState)) {
			closeMenu(body,topnav);
		} else {
			topnav.classList.add(menuState);
			body.classList.add(menuState);
			moveSiteNavIntoGuideNav(topnav);
		}
		// TODO: add click-detect when opening menu (prev closed), remove it when menu is closed.
		// Menu is now hidden, remove click listner for closing it from body-click
		/*
		document.getElementsByTagName("html")[0].removeEventListener('click', function() {
			toggleFFUgameMenuOff(topnav,siteBlock);
			return false;
		});
		*/
	});

	// Also click on the mask to close the menu
	body.querySelector('.ffu_tn5_body-mask').addEventListener('click', function() {
		if (body.classList.contains('menu-open')) {
			closeMenu(body,topnav);
		}
	});
	document.onkeydown = function(evt) {
	    evt = evt || window.event;
	    var isEscape = false;
	    if ("key" in evt) {
	        isEscape = (evt.key == "Escape" || evt.key == "Esc");
	    } else {
	        isEscape = (evt.keyCode == 27);
	    }
	    if (isEscape) {
			if (body.classList.contains('menu-open')) {
		        closeMenu(body,topnav);
			}
	    }
	};
}

function closeMenu(body,topnav) {
	topnav.classList.remove(menuState);
	body.classList.remove(menuState);
	moveSiteNavBack(topnav);
}


// Topnav main functionality
function fetchTopNavJson(topnav) {
 	var curUrl = document.URL;
//	var topnav = document.getElementsByClassName('ffu_tn5_wrapper');
//	if (topnav.length) { topnav = topnav[0]; }

//	if (topnav) {
		var sourceUrl = topnav.dataset.source;

	 	// Fetch JSON needed for TopNav
		var request = new XMLHttpRequest();
		request.open('GET', sourceUrl + "?ref=" + encodeURIComponent(curUrl), true);
		request.withCredentials = true;
		request.onload = function() {
		  if (request.status >= 200 && request.status < 400) {
		    // Success!
		    var data = JSON.parse(request.responseText);
				populateTopNav(data,topnav);
		  } else {
		    // We reached our target server, but it returned an error
		  }
		};
		request.onerror = function() {
		  // There was a connection error of some sort
		};
		request.send();

//	}

}

function populateTopNav(json,topnav) {
	// Only run if we've fetched the json
	if (json) {
		// Guide-related
		// *****************
		if (json.updates) {
			/*
			 * Read cookie before rendering html, only count new things as new
			 * Count number of items (total)
			 * Add ".active-events" inside ".topnav-games-item"
			 * Insert a strong (or label) with a counter
			 * Click-event for toggling it?
			 * When expanded, show date + title + link to each item
			 * Option somewhere to hide this info (dismiss-function, would need to store in cookie)
			 * First solution could be to add this badge only to "guides" meny item (sites)
			 */
		}
		// News-related
		// *****************
		if (json.news) {
			/*
			 * todo...
			 */
		}
		// Blog-related
		// *****************
		if (json.blogs) {
			/*
			 * todo...
			 */
		}

	} // if json

}

// https://davidwalsh.name/javascript-debounce-function
// Here's the basic JavaScript debounce function (as taken from Underscore.js):

// Returns a function, that, as long as it continues to be invoked, will not
// be triggered. The function will be called after it stops being called for
// N milliseconds. If `immediate` is passed, trigger the function on the
// leading edge, instead of the trailing.
function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
};
