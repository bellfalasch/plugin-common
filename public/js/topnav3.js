// Mozilla, Opera, Webkit
if ( document.addEventListener ) {
  document.addEventListener( "DOMContentLoaded", function(){
    document.removeEventListener( "DOMContentLoaded", arguments.callee, false);
    domReady();
  }, false );

// If IE event model is used
} else if ( document.attachEvent ) {
  // ensure firing before onload
  document.attachEvent("onreadystatechange", function(){
    if ( document.readyState === "complete" ) {
      document.detachEvent( "onreadystatechange", arguments.callee );
      domReady();
    }
  });
}

function domReady () {
	fetchTopNavJson();
};

// Topnav main functionality
function fetchTopNavJson() {
 	var curUrl = document.URL;
	var topnav = document.getElementById('TopNav');

	if (topnav) {
		var sourceUrl = topnav.dataset.source;

	 	// Fetch JSON needed for TopNav
		var request = new XMLHttpRequest();
		request.open('GET', sourceUrl + "?ref=" + encodeURIComponent(curUrl), true);
		request.withCredentials = true;
		request.onload = function() {
		  if (request.status >= 200 && request.status < 400) {
		    // Success!
		    var data = JSON.parse(request.responseText);
				populateTopNav(data,topnav);
		  } else {
		    // We reached our target server, but it returned an error
		  }
		};
		request.onerror = function() {
		  // There was a connection error of some sort
		};
		request.send();

		// Mobile TopNav
		var maxWidth = 760; // Reflects CSS media where we go into tablet/mobile-mode
		var className = "opened";
		var isMobile = window.matchMedia("only screen and (max-width: " + maxWidth + "px)");
		if (isMobile) {
      var siteBlock = topnav.querySelector(".block.site");
/*
			// Add class when on small screens, will hide menu
			if (siteBlock.classList) {
			  siteBlock.classList.add(className);
			} else {
			  siteBlock.className += ' ' + className;
			}
*/
			// Let click toggle menu on and off
			topnav.querySelector(".ffu_topnav_menu_toggle-on").addEventListener('click', function() {
				//topnav.querySelector("nav.games:before").style.display = "block";
        if (topnav.classList) {
          topnav.classList.add("menuopen-games");
        } else {
          topnav.className += ' ' + "menuopen-games";
        }
        if (siteBlock.classList) {
          siteBlock.classList.add(className);
        } else {
          siteBlock.className += ' ' + className;
        }
        // Enable clicking outside body to close the menu
/*
        document.getElementsByTagName("html")[0].addEventListener('click', function() {
          toggleFFUgameMenuOff(topnav,siteBlock);
        });
*/
        return false;
			});
      topnav.querySelector(".ffu_topnav_menu_toggle-off").addEventListener('click', function() {
        toggleFFUgameMenuOff(topnav,siteBlock);
        return false;
      });
		}

	}

}

function toggleFFUgameMenuOff(topnav,siteBlock) {
  var className;
  className = "menuopen-games";
  if (topnav.classList) {
    topnav.classList.remove(className);
  } else {
    topnav.className = topnav.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
  }
  className = "opened";
  if (siteBlock.classList) {
    siteBlock.classList.remove(className);
  } else {
    siteBlock.className = siteBlock.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
  }
  // Menu is now hidden, remove click listner for closing it from body-click
  document.getElementsByTagName("html")[0].removeEventListener('click', function() {
    toggleFFUgameMenuOff(topnav,siteBlock);
    return false;
  });
}

function populateTopNav(json,topnav) {
	// Fetch user data
	var cookie, username, onlineSince, online;
	if (json) {
		if (json.ffu) {
			if (json.ffu.user) {

				cookie = json.ffu.user.cookie || "";
				username = json.ffu.user.username || "";
				onlineSince = json.ffu.user.online_since || "";

				online = username != "" ? true : false;

				// Logged in, so show user menu
				var communityDOM = topnav.querySelector(".block.community");
				if (communityDOM) {
					if (online) {
						topnav.querySelector(".online.true").style.display = 'block';
						topnav.querySelector(".online.false").style.display = 'none';
						var now = json.ffu["last-fetched"]; // Assume last fetch is now (to escape back-end vs front-end time differences)
						var onlineString = calculateOnlineTime(onlineSince, now);
						communityDOM.querySelector(".user .onlinetime").innerHTML = onlineString;
						communityDOM.querySelector(".user .username").innerHTML = username;
					} else {
						communityDOM.querySelector(".online.false .input.username").value = cookie;
					}
				}
			}
		}
	}

	// Fetch news/update data from json
	var news_date, update_date, news_title, update_title;
	if (json) {
		if (json.ffu) {
			if (json.ffu.news) {
				news_title = json.ffu.news.title;
				news_url = json.ffu.news.link
				news_date = json.ffu.news.date;
			}
			if (json.ffu.updates) {
				update_title = json.ffu.updates.title;
				update_url = json.ffu.updates.link;
				update_date = json.ffu.updates.date;
			}
		}
	}

	// Populate news ticker
	var TOPNAV_date = "",
			TOPNAV_title = "",
			TOPNAV_url = "";

	if ( update_date > news_date ) {
		TOPNAV_title = update_title;
		TOPNAV_url = update_url;
		TOPNAV_date = update_date;
	} else {
		TOPNAV_title = news_title;
		TOPNAV_url = news_url;
		TOPNAV_date = news_date;
	}

	topnav.querySelector(".newsticker .news_datetime").innerHTML = "(" + TOPNAV_date + ")";
	var newsLink = topnav.querySelector(".newsticker .news_link");
	newsLink.innerHTML = TOPNAV_title;
	newsLink.setAttribute("href", TOPNAV_url);
}

// Find out how long the user's been logged in
function calculateOnlineTime(onlineSince, now) {
//	console.log("data in - onlineSince: " + onlineSince);
//	console.log("data in - now: " + now);
	var onlinefra = new Date(onlineSince + "+0200"); // Force correct timezone
	var onlineto = new Date(now + "+0200");
//	console.log("onlinefra: " + onlinefra);
//	console.log("onlineto: " + onlineto);

	var diffMs = Math.abs(onlineto - onlinefra); // Milliseconds
	var minutes = Math.floor( (diffMs / 1000) / 60 );
//	console.log("diffMs: " + diffMs);
//	console.log("minutes: " + minutes);

	var end_string;
	if ( minutes > 60 ) {
		var hours = Math.floor(minutes / 60);
		var strMinutes = minutes - (hours * 60);
		end_string = hours.toString() + ":" + strMinutes.toString();
	} else {
		end_string = "0:" + minutes.toString();
	}
//	console.log("end_string: " + end_string);
	return end_string;
}
