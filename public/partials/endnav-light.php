<?php
/**
 * @link       https://guide.ffuniverse.nu
 * @since      1.5.0
 *
 * @package    Ffuniverse_Nu
 * @subpackage Ffuniverse_Nu/public/partials
 */
?>
<?php
    // ************************************************************************
    // * ENDNAV Light - PHP
    // ************************************************************************

    // Our endnav should only contain bare minimum serverside code. Only a hook needed in the template file.
    $FFU_ROOT = ".ffuniverse.nu/";
    $http = "http://";
    $https = "https://";
    $urlMain  = $http . "www" . $FFU_ROOT;
    $urlGuide = $https . "guide" . $FFU_ROOT;
    $urlNews  = $https . "nyheter" . $FFU_ROOT;
    $urlDev   = $https . "dev" . $FFU_ROOT;
    $urlAbout = $https . "om" . $FFU_ROOT;
    // "?utm_source=TopNav5-wp&amp;utm_campaign=FFU2.5&amp;utm_medium=";
    $utmPages = ""; //$utmRoot . "SiteList"; -- deactivate, use heatmap plugins instead
    $utmGuides = ""; //$utmRoot . "GameList"; -- deactivate, use heatmap plugins instead
    $utm_footer_sites = "";
    $utm_footer_about = "";
?>

<footer class="ffu_endnav_wrapper">
  <div class="container">
    <p class="centered">
        PS. Vi finns också på
        <a href="https://twitter.com/FFUniverse_nu" rel="nofollow">X (Twitter)</a> och
        <a href="https://www.facebook.com/finalfantasy.nu" rel="nofollow">Facebook</a> och
        <a href="https://instagram.com/FFUniverse" rel="nofollow">Instagram</a> och 
        <a href="https://open.spotify.com/user/bellfalasch/playlist/7Mz2lVCJg3O5BW9bfo0JDs" rel="nofollow">Spotify</a> och
        <a href="https://www.youtube.com/channel/UCv1qhT8C6LnQ5hpC-YDa75A" rel="nofollow">Youtube</a> och
        <a href="https://discord.gg/ncGqDWC" rel="nofollow">Discord</a>!
    </p>
    <div class="main-sites">
      <ul class="flex-row">
        <li><a href="<?= $urlGuide ?>" title="Visa alla v&aring;ra Final Fantasy och Square Enix-guider">Guider</a></li>
        <li><a href="<?= $urlNews ?>" title="Vår nyhetssajt, sista nytt om allt från Square Enix">Nyheter</a></li>
        <li><a href="<?= $urlMain ?>community/" title="Bli medlem och diskutera FF med andra fans.">Community</a></li>
        <li><a href="<?= $urlAbout ?>" title="Vanliga frågor och annan info om oss och vår sajt.">Om FFU</a></li>
        <li><a href="<?= $urlAbout ?>blogg/" title="En tekniskt inriktad blogg om utvecklingen bakom sajten.">Blogg</a></li>
      </ul>
    </div>
    <div class="additional-info">
      <ul class="flex-row">
        <li class="full-width"><a href="<?= $urlAbout ?>copyright/">Copyright 1997-<?= date("Y"); ?>  - FFUniverse.nu</a></li>
        <li><a href="<?= $urlAbout ?>hjalp-oss/">Jobba för oss</a></li>
        <li><a href="<?= $urlAbout ?>om-oss/">Om oss</a></li>
        <li><a href="<?= $urlAbout ?>anvandarvillkor/">Anv&auml;ndarvillkor</a></li>
        <li><a href="<?= $urlAbout ?>cookies/">Cookies</a></li>
      </ul>
    </div>
  </div>
</footer>
