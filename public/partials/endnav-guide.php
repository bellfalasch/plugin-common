<?php
/**
 * @link       https://guide.ffuniverse.nu/
 * @since      1.5.0
 *
 * @package    Ffuniverse_Nu
 * @subpackage Ffuniverse_Nu/public/partials
 */
?>
<?php
	// ************************************************************************
	// * ENDNAV guide (full) - PHP
	// ************************************************************************

  	// Our endnav should only contain bare minimum serverside code. Only a hook needed in the template file.
	$FFU_ROOT = ".ffuniverse.nu/";
	$http = "http://";
	$https = "https://";
	$urlMain  = $http . "www" . $FFU_ROOT;
	$urlGuide = $https . "guide" . $FFU_ROOT;
	$urlNews  = $https . "nyheter" . $FFU_ROOT;
	$urlDev   = $https . "dev" . $FFU_ROOT;
	$urlAbout = $https . "om" . $FFU_ROOT;
	// "?utm_source=endnav-guide&amp;utm_medium=EndNavLinks&amp;utm_campaign=FFU2.5";
	// Using these groups for medium: EndNavLinks, InfoLinks, GuideLinksPopular, SiteLinks
	$utmPages = ""; //$utmRoot . "SiteList"; -- deactivate, use heatmap plugins instead
	$utmGuides = ""; //$utmRoot . "GameList"; -- deactivate, use heatmap plugins instead

?>

<footer class="ffu_endnav_guide">
	<div class="wrapper">
		<div class="content">
			<div class="container-grid"> 
				<div class="box ffu">
					<div class="bullet">
						<p>
							Final Fantasy Universe<br />
							Online sedan 1997
						</p>
					</div>
					<a href="<?= $urlAbout ?>copyright/" rel="nofollow">Copyright 1997 - <?= date("Y"); ?></a>
					<p>
						<a href="<?= $urlAbout ?>anvandarvillkor/" rel="nofollow">Användarvillkor</a>
						<a href="<?= $urlAbout ?>om-oss/" rel="nofollow">Om oss</a><br />
						<a href="<?= $urlAbout ?>cookies/" rel="nofollow">Cookies</a>
						<?php /*<a href="<?= $urlAbout ?>sitemap/" rel="nofollow">Sidekarta</a>*/ ?>
						<?php /*<a href="<?= $urlAbout ?>annonsera-hos-oss/" rel="nofollow">Annonsera</a>*/ ?>
						<a href="<?= $urlAbout ?>hjalp-oss/">Jobba för oss</a>
					</p>
				</div>
				<div class="box sites">
					<h4>Våra sidor:</h4>
					<ul>
						<li><a href="<?= $urlNews ?>" title="Nyheter och recensioner om alla Square Enix-spel">Spelnyheter</a></li>
						<li><a href="<?= $urlGuide ?>" title="Visa alla v&aring;ra Final Fantasy och Square Enix-guider">Guider</a></li>
						<li><a href="<?= $urlMain ?>community/" title="Bli medlem och prata med andra Final Fantasy-fans">Community</a></li>
						<li><a href="<?= $urlAbout ?>" title="Vanliga frågor och annan info om oss och vår sajt.">Om FFU</a></li>
						<li><a href="<?= $urlAbout ?>blogg/" title="En tekniskt inriktad blogg om utvecklingen bakom sajten.">Vår blogg</a></li>
					</ul>
				</div>
				<div class="box guides">
					<h4>Populära guider:</h4>
					<ul>
						<li><a href="<?= $urlGuide ?>ff8/">Final Fantasy VIII</a></li>
						<li><a href="<?= $urlGuide ?>ff7/">Final Fantasy VII</a></li>
						<li><a href="<?= $urlGuide ?>ff10/">Final Fantasy X</a></li>
						<li><a href="<?= $urlGuide ?>ff9/">Final Fantasy IX</a></li>
						<li><a href="<?= $urlGuide ?>kingdom-hearts/">Kingdom Hearts</a></li>
						<li><a href="<?= $urlGuide ?>ff4/">Final Fantasy IV</a></li>
					</ul>
					<a href="<?= $urlGuide ?>" class="more-games" title="Vi har guider till 24 olika spel, på över 1000 sidor totalt!">Se alla spelguider</a>
				</div>
				<div class="box social">
					<h4>Också på:</h4>
					<ul>
						<li class="tw"><a href="https://www.twitter.com/FFUniverse_nu/" rel="nofollow">X (Twitter)</a></li>
						<li class="fb"><a href="https://www.facebook.com/finalfantasy.nu/" rel="nofollow">Facebook</a></li>
						<li class="insta"><a href="https://instagram.com/FFUniverse" rel="nofollow">Instagram</a></li>
						<li class="discord"><a href="https://discord.gg/ncGqDWC" rel="nofollow">Discord</a></li>
						<li class="youtube"><a href="https://www.youtube.com/channel/UCv1qhT8C6LnQ5hpC-YDa75A" rel="nofollow">Youtube</a></li>
						<li class="spotify"><a href="https://open.spotify.com/user/bellfalasch/playlist/7Mz2lVCJg3O5BW9bfo0JDs" rel="nofollow">Spotify</a></li>
						<li class="rss"><a href="<?= $urlAbout ?>rss-feeds/" rel="nofollow">RSS</a></li>
					</ul>
				</div>
				<div class="box who-are-we">
					<div class="message">
						<h4>Vad är FFU?</h4>
						<p>
							Final Fantasy Universe har som fokus att producera egenskrivna
							guider, tips, och information - helt på svenska.
						</p>
						<a href="<?= $urlAbout ?>om-oss/" rel="nofollow">Läs mer</a>
					</div>
				</div>
			</div><!-- .container-grid -->
		</div><!-- .content -->
	</div><!-- .wrapper -->
</footer>
