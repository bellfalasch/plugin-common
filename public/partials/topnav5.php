<?php
/**
 * @link       https://guide.ffuniverse.nu/
 * @since      1.1.0
 *
 * @package    Ffuniverse_Nu
 * @subpackage Ffuniverse_Nu/public/partials
 */
?>
<?php
	// ************************************************************************
	// * TOPNAV - PHP
	// * Detta är enbart HTML-kod. Denna TopNav använder vårt nya system för att
	// * hämta alla data från JSON istället för från databasen och Sessions. På
	// * detta sätt kan vi läsa serverspecifik data över hela FFU.
	// ************************************************************************

  // Our topnav should only contain bare minimum serverside code. Only a hook needed in the template file.
	$FFU_ROOT = ".ffuniverse.nu/";
	$http = "http://";
	$https = "https://";
	$urlMain  = $http . "www" . $FFU_ROOT;
	$urlGuide = $https . "guide" . $FFU_ROOT;
	$urlNews  = $https . "nyheter" . $FFU_ROOT;
	$urlDev   = $https . "dev" . $FFU_ROOT;
	$urlAbout = $https . "om" . $FFU_ROOT;
	$source = $urlGuide . "_cache_json.php";
	// "?utm_source=TopNav5-wp&amp;utm_campaign=FFU2.5&amp;utm_medium=";
	$utmPages = ""; //$utmRoot . "SiteList"; -- deactivate, use heatmap plugins instead
	$utmGuides = ""; //$utmRoot . "GameList"; -- deactivate, use heatmap plugins instead

?>

<div class="ffu_tn5_body-mask"></div>
<header class="ffu_tn5_wrapper" data-source="<?= $source ?>">

	<button class="ffu_tn5_menu_toggler hamburger hamburger--emphatic" type="button" aria-label="Menu" aria-controls="ffu_topnav">
		<span class="hamburger-box">
			<span class="hamburger-inner"></span>
		</span>
	</button>

	<div class="ffu_tn5_sites_container">
		<div class="ffu_tn5_sites_block">

			<a href="<?= $urlMain ?>" class="ffu_tn5_logo">
				<strong>FFU</strong><small>niverse</small>
			</a>

			<nav role="navigation" class="sites">
				<ol>
					<li><a href="<?= $urlGuide . $utmPages ?>" title="Visa alla v&aring;ra Final Fantasy-guider">Guider</a></li>
					<li><a href="<?= $urlNews . $utmPages ?>" title="Nyheter om Final Fantasy och Square Enix.">Nyheter</a></li>
					<li><a href="<?= $urlMain ?>community/<?=  $utmPages ?>" title="Community med forum, chatt och spel f&ouml;r medlemmar.">Community</a></li>
					<li><a href="<?= $urlAbout . $utmPages ?>" title="Info om sajten, kontakt, etc.">Om oss</a></li>
					<li><a href="<?= $urlAbout ?>blogg/<?= $utmPages ?>" title="V&aring;r blogg, om utveckling av FFU.">Blogg</a></li>
				</ol>
			</nav>

			<div class="community-part"></div>

		</div>
	</div>

	<div class="ffu_tn5_games_container">
		<nav role="navigation" class="ffu_tn5_games_block" id="ffu_topnav">
			<span class="menu-label">Final Fantasy - grundserien</span>
			<ol class="ffu_tn5_guides_main-series">
				<?php /*
					SEMANTICS:
					- link's title attribute is full title, every word written out. This can have any length.
					- inside link <span> with class "full-name" decides what to write in the dropdown menu (some on desktop, all on mobile). Beware, not all long titles can fit!
					- inside link <strong> with class "short-name" decides what to show in TopNav on desktop.
					- inside link <abbr> added for "meta-tagging" a game with its most commonly used abbrivation (if applicable). It is never shown.
				*/ ?>
				<li class="topnav-games-item">
					<a title="Final Fantasy I" href="<?= $urlGuide ?>ff1/">
						<span class="full-name">Final Fantasy I</span>
						<strong class="short-name">I</strong>
						<abbr>FF1</abbr>
					</a>
				</li>
				<li class="topnav-games-item">
					<a title="Final Fantasy II" href="<?= $urlGuide ?>ff2/">
						<span class="full-name">Final Fantasy II</span>
						<strong class="short-name">II</strong>
						<abbr>FF2</abbr>
					</a>
				</li>
				<li class="topnav-games-item">
					<a title="Final Fantasy III" href="<?= $urlGuide ?>ff3/">
						<span class="full-name">Final Fantasy III</span>
						<strong class="short-name">III</strong>
						<abbr>FF3</abbr>
					</a>
				</li>
				<li class="topnav-games-item">
					<a title="Final Fantasy IV" href="<?= $urlGuide ?>ff4/">
						<span class="full-name">Final Fantasy IV</span>
						<strong class="short-name">IV</strong>
						<abbr>FF4</abbr>
					</a>
				</li>
				<li class="topnav-games-item">
					<a title="Final Fantasy V" href="<?= $urlGuide ?>ff5/">
						<span class="full-name">Final Fantasy V</span>
						<strong class="short-name">V</strong>
						<abbr>FF5</abbr>
					</a>
				</li>
				<li class="topnav-games-item">
					<a title="Final Fantasy VI" href="<?= $urlGuide ?>ff6/">
						<span class="full-name">Final Fantasy VI</span>
						<strong class="short-name">VI</strong>
						<abbr>FF6</abbr>
					</a>
				</li>
				<li class="topnav-games-item">
					<a title="Final Fantasy VII" href="<?= $urlGuide ?>ff7/">
						<span class="full-name">Final Fantasy VII</span>
						<strong class="short-name">VII</strong>
						<abbr>FF7</abbr>
					</a>
				</li>
				<li class="topnav-games-item">
					<a title="Final Fantasy VIII" href="<?= $urlGuide ?>ff8/">
						<span class="full-name">Final Fantasy VIII</span>
						<strong class="short-name">VIII</strong>
						<abbr>FF8</abbr>
					</a>
				</li>
				<li class="topnav-games-item">
					<a title="Final Fantasy IX" href="<?= $urlGuide ?>ff9/">
						<span class="full-name">Final Fantasy IX</span>
						<strong class="short-name">IX</strong>
						<abbr>FF9</abbr>
					</a>
				</li>
				<li class="topnav-games-item">
					<a title="Final Fantasy X" href="<?= $urlGuide ?>ff10/">
						<span class="full-name">Final Fantasy X</span>
						<strong class="short-name">X</strong>
						<abbr>FF10</abbr>
					</a>
				</li>
				<li class="topnav-games-item">
					<a title="Final Fantasy XI" href="<?= $urlGuide ?>ff11/">
						<span class="full-name">Final Fantasy XI</span>
						<strong class="short-name">XI</strong>
						<abbr>FF11</abbr>
					</a>
				</li>
				<li class="topnav-games-item">
					<a title="Final Fantasy XII" href="<?= $urlGuide ?>ff12/">
						<span class="full-name">Final Fantasy XII</span>
						<strong class="short-name">XII</strong>
						<abbr>FF12</abbr>
					</a>
				</li>
				<li class="topnav-games-item">
					<a title="Final Fantasy XIII" href="<?= $urlGuide ?>ff13/">
						<span class="full-name">Final Fantasy XIII</span>
						<strong class="short-name">XIII</strong>
						<abbr>FF13</abbr>
					</a>
				</li>
				<?php
				/* Ta bort för att få plats med FF7R
				<li class="topnav-games-item disabled">
					<span class="full-name">Final Fantasy XIV</span>
					<strong class="short-name">XIV</strong>
					<abbr>FF14</abbr>
				</li>
				*/ ?>
				<li class="topnav-games-item">
					<a title="Final Fantasy XV (tidigare FF Versus XIII)" href="<?= $urlGuide ?>ff15/">
						<span class="full-name">Final Fantasy XV</span>
						<strong class="short-name">XV</strong>
						<abbr>FF15</abbr>
					</a>
				</li>
			</ol>

			<span class="menu-label">Final Fantasy - övriga spel</span>
			<ol class="ffu_tn5_guides_spinoffs">
				<li class="topnav-games-item">
					<a title="Final Fantasy VII Remake" href="<?= $urlGuide ?>ff7-remake/">
						<span class="full-name">FFVII Remake</span>
						<strong class="short-name">FF7R</strong>
						<abbr>FF7R</abbr>
					</a>
				</li>
				<li class="topnav-games-item">
					<a title="Final Fantasy X-2" href="<?= $urlGuide ?>ff10-2/">
						<span class="full-name">Final Fantasy X-2</span>
						<strong class="short-name">X-2</strong>
						<abbr>FF10-2</abbr>
					</a>
				</li>
				<li class="topnav-games-item">
					<a title="Final Fantasy Crystal Chronicles" href="<?= $urlGuide ?>ff-crystal-chronicles/">
						<span class="full-name">FF Crystal Chronicles</span>
						<strong class="short-name">CC</strong>
						<abbr>FFCC</abbr>
					</a>
				</li>
				<li class="topnav-games-item">
					<a title="Final Fantasy Tactics Advance" href="<?= $urlGuide ?>ff-tactics-advance/">
						<span class="full-name">FF Tactics Advance</span>
						<strong class="short-name">TA</strong>
						<abbr>FFTA</abbr>
					</a>
				</li>
			</ol>

			<span class="menu-label">Square Enix - övriga spel</span>
			<ol class="ffu_tn5_guides_other-games">
				<li class="topnav-games-item more-games">
					<button class="menu-label" type="button" aria-label="Menu" aria-controls="ffu_topnav_additional-guides">Fler spelguider</button>
					<ol class="topnav-game-dropdown" id="ffu_topnav_additional-guides">
						<li class="topnav-games-item">
							<a title="Bravely Default" href="<?= $urlGuide ?>bravely-default/">
								<span class="full-name">Bravely Default</span>
								<strong class="short-name">BD</strong>
								<abbr>BD</abbr>
							</a>
						</li>
						<li class="topnav-games-item">
							<a title="Kingdom Hearts" href="<?= $urlGuide ?>kingdom-hearts/">
								<span class="full-name">Kingdom Hearts</span>
								<strong class="short-name">KH</strong>
								<abbr>KHI</abbr>
							</a>
						</li>
						<li class="topnav-games-item">
							<a title="Chrono Trigger" href="<?= $urlGuide ?>chrono-trigger/">
								<span class="full-name">Chrono Trigger</span>
								<strong class="short-name">CT</strong>
								<abbr>CT</abbr>
							</a>
						</li>
						<li class="topnav-games-item">
							<a title="Brave Fencer Musashi" href="<?= $urlGuide ?>brave-fencer-musashi/">
								<span class="full-name">Brave Fencer Musashi</span>
								<strong class="short-name">BFM</strong>
								<abbr>BFM</abbr>
							</a>
						</li>
					</ol>
				</li>
			</ol>

		</nav>
	</div>

</header>
