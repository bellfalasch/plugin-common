<?php
/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 * This file should primarily consist of HTML with a little bit of PHP.
 *
 * @link       https://guide.ffuniverse.nu/
 * @since      1.0.0
 *
 * @package    Ffuniverse_Nu
 * @subpackage Ffuniverse_Nu/public/partials
 */
?>
<?php
	// ************************************************************************
	// * TOPNAV - PHP
	// * Detta är enbart HTML-kod. Denna TopNav använder vårt nya system för att
	// * hämta alla data från JSON istället för från databasen och Sessions. På
	// * detta sätt kan vi läsa serverspecifik data över hela FFU.
	// ************************************************************************


//	$path_to_json = "http://www.ffuniverse.nu/__topnav_json.asp";
/*
	$rawdata = file_get_contents( $path_to_json );
	$rawdata = trim( utf8_encode( $rawdata ) );
	//echo $rawdata;
	//echo utf8_encode( $rawdata );

	$json = json_decode($rawdata, true);
*/
//	$json = $json[0];
//	var_dump( $json );

  // Our topnav should only contain bare minimum topnav code, but don't forget the full width div with bg as it is part of TopNav design!
	$protocol = "http://";
	$noNewsText = "Läs våra senaste nyheter om Square Enix och Final Fantasy";
	$urlGuide = "https://guide.ffuniverse.nu/";
	$urlMain = $protocol . "www.ffuniverse.nu/";
	$source = $urlMain . "__topnav_json.asp";

?>
<div class="ffu_topnav_fluid_wrapper">

<div id="TopNav" data-source="<?= $source ?>">

	<div class="ffu_logo">
		<a href="<?= $urlMain ?>">
			<strong>FFU</strong><span>niverse</span>
		</a>
	</div>
	<a class="ffu_topnav_menu_toggle-on" href="#">+ fler guider</a>
	<a class="ffu_topnav_menu_toggle-off" href="#">&times;</a>

	<div class="block site">

		<nav role="navigation" class="games">
			<ul>
				<li>
					<a  style="width:11px" title="Final Fantasy I"
						href="<?= $urlGuide ?>ff1/?utm_source=TopNav-wp&amp;utm_medium=GameList&amp;utm_campaign=FFU2.5">
						Final Fantasy I <small>(FF1)</small>
					</a>
				</li>
				<li>
					<a style="width:24px" title="Final Fantasy II"
						href="<?= $urlGuide ?>ff2/?utm_source=TopNav-wp&amp;utm_medium=GameList&amp;utm_campaign=FFU2.5">
						Final Fantasy II <small>(FF2)</small>
					</a>
				</li>
				<li>
					<a style="width:26px" title="Final Fantasy III"
						href="<?= $urlGuide ?>ff3/?utm_source=TopNav-wp&amp;utm_medium=GameList&amp;utm_campaign=FFU2.5">
						Final Fantasy III <small>(FF3)</small>
					</a>
				</li>
				<li>
					<a style="width:28px" title="Final Fantasy IV"
						href="<?= $urlGuide ?>ff4/?utm_source=TopNav-wp&amp;utm_medium=GameList&amp;utm_campaign=FFU2.5">
						Final Fantasy IV <small>(FF4)</small>
					</a>
				</li>
				<li>
					<a style="width:20px" title="Final Fantasy V"
						href="<?= $urlGuide ?>ff5/?utm_source=TopNav-wp&amp;utm_medium=GameList&amp;utm_campaign=FFU2.5">
						Final Fantasy V <small>(FF5)</small>
					</a>
				</li>
				<li>
					<a style="width:28px" title="Final Fantasy VI"
						href="<?= $urlGuide ?>ff6/?utm_source=TopNav-wp&amp;utm_medium=GameList&amp;utm_campaign=FFU2.5">
						Final Fantasy VI <small>(FF6)</small>
					</a>
				</li>
				<li>
					<a style="width:34px" title="Final Fantasy VII"
						href="<?= $urlGuide ?>ff7/?utm_source=TopNav-wp&amp;utm_medium=GameList&amp;utm_campaign=FFU2.5">
						Final Fantasy VII <small>(FF7)</small>
					</a>
				</li>
				<li>
					<a style="width:41px" title="Final Fantasy VIII"
						href="<?= $urlGuide ?>ff8/?utm_source=TopNav-wp&amp;utm_medium=GameList&amp;utm_campaign=FFU2.5">
						Final Fantasy VIII <small>(FF8)</small>
					</a>
				</li>
				<li>
					<a style="width:25px" title="Final Fantasy IX"
						href="<?= $urlGuide ?>ff9/">
						Final Fantasy IX <small>(FF9)</small>
					</a>
				</li>
				<li>
					<a style="width:22px" title="Final Fantasy X"
						href="<?= $urlGuide ?>ff10/">
						Final Fantasy X <small>(FF10)</small>
					</a>
				</li>
				<li>
					<a style="width:29px" title="Final Fantasy XI"
						href="<?= $urlGuide ?>ff11/?utm_source=TopNav-wp&amp;utm_medium=GameList&amp;utm_campaign=FFU2.5">
						Final Fantasy XI <small>(FF11)</small>
					</a>
				</li>
				<li>
					<a style="width:35px" title="Final Fantasy XII"
						href="<?= $urlGuide ?>ff12/?utm_source=TopNav-wp&amp;utm_medium=GameList&amp;utm_campaign=FFU2.5">
						Final Fantasy XII <small>(FF12)</small>
					</a>
				</li>
				<li style="margin-right:40px;">
					<a style="width:42px" title="Final Fantasy XIII"
						href="<?= $urlGuide ?>ff13/?utm_source=TopNav-wp&amp;utm_medium=GameList&amp;utm_campaign=FFU2.5">
						Final Fantasy XIII <small>(FF13)</small>
					</a>
				</li>
				<li style="margin-right:14px" title="Final Fantasy XV (tidigare FF Versus XIII)">
					<a  style="width:26px"
						href="<?= $urlGuide ?>ff15/?utm_source=TopNav-wp&amp;utm_medium=GameList&amp;utm_campaign=FFU2.5">
						Final Fantasy XV <small>(FF15)</small>
					</a>
				</li>
				<li>
					<a  style="width:32px" title="Final Fantasy X-2"
						href="<?= $urlMain ?>ffx2/default.asp">
						Final Fantasy X-2 <small>(FF10 - 2)</small>
					</a>
				</li>
				<li>
					<a style="width:25px" title="Bravely Default"
						href="<?= $urlGuide ?>bravely-default/?utm_source=TopNav-wp&amp;utm_medium=GameList&amp;utm_campaign=FFU2.5">
						Bravely Default
					</a>
				</li>
				<li style="margin-right:7px" title="Kingdom Hearts">
					<a  style="width:30px"
						href="<?= $urlGuide ?>kingdom-hearts/?utm_source=TopNav-wp&amp;utm_medium=GameList&amp;utm_campaign=FFU2.5">
						Kingdom Hearts
					</a>
				</li>
				<li class="topnav_more-games">
					<a style="width:55px" href="<?= $urlGuide ?>?utm_source=TopNav-wp&amp;utm_medium=GameList&amp;utm_campaign=FFU2.5" title="Visa alla v&aring;ra spelsajter!">
						Visa fler spel!
					</a>
				</li>
			</ul>
		</nav>

		<nav role="navigation" class="pages">
			<ul>
				<li><a href="https://nyheter.ffuniverse.nu/?utm_source=TopNav-wp&amp;utm_medium=PagesList&amp;utm_campaign=FFU2.5" title="Nyheter om Final Fantasy och Square Enix.">Nyheter</a></li>
				<li><a href="<?= $urlMain ?>community/" title="Community med forum, chatt och spel f&ouml;r medlemmar.">Community</a></li>
				<li><a href="<?= $urlGuide ?>?utm_source=TopNav-wp&amp;utm_medium=PagesList&amp;utm_campaign=FFU2.5" title="Visa alla v&aring;ra Final Fantasy-guider" class="topnav_more-ff">Final Fantasy</a></li>
				<li><a href="https://om.ffuniverse.nu/?utm_source=TopNav-wp&amp;utm_medium=PagesList&amp;utm_campaign=FFU2.5" title="Info om sajten, kontakt, etc.">Om oss</a></li>
				<li><a href="https://dev.ffuniverse.nu/?utm_source=TopNav-wp&amp;utm_medium=PagesList&amp;utm_campaign=FFU2.5" title="V&aring;r blogg, om utveckling av FFU.">Blogg</a></li>
			</ul>
		</nav>

	</div>

	<div class="block community">

		<div class="online false">

			<form method="post" action="<?= $urlMain ?>community/user_loginconfirm.asp" class="login">
				<label for="cv3_user" class="icon username">Anv&auml;ndarnamn</label>
				<input type="text" name="cv3_user" id="cv3_user" class="input username" maxlength="25" value="" />
				<label for="cv3_pass" class="icon password">L&ouml;senord</label>
				<input type="password" name="cv3_pass" id="cv3_pass" class="input password" maxlength="20" />
				<input type="submit" title="Logga in" class="icon submit" value="" />
			</form>

			<a href="<?= $urlMain ?>community/default_adduser.asp" class="join_us" title="Bli medlem h&auml;r - gratis och enkelt!">Bli medlem</a>

		</div>

		<div class="online true">

			<div class="user">
			  <a href="<?= $urlMain ?>community/user_user.asp" class="username">Inloggad som ...</a><br />
			  Inloggningstid: <strong class="onlinetime">??</strong>
			</div>

			<div class="links">|
			  <a href="<?= $urlMain ?>community/user_start.asp" accesskey="s" title="Snabbknapp: Alt + S">Start</a> |
			  <a href="<?= $urlMain ?>community/user_pics.asp" accesskey="p" title="Snabbknapp: Alt + P">Bilder</a> |
			  <a href="<?= $urlMain ?>community/user_logout.asp" accesskey="q" title="Snabbknapp: Alt + Q">Logga ut</a> |<br />|
			  <a href="<?= $urlMain ?>community/mognet_inbox.asp" accesskey="m" title="Snabbknapp: Alt + M">Mognet</a> |
			  <a href="<?= $urlMain ?>community/chatt_chat.asp?do=login" accesskey="c" title="Snabbknapp: Alt + C">Chatt</a> |
			  <a href="<?= $urlMain ?>community/forum_start.asp" accesskey="f" title="Snabbknapp: Alt + F">Forum</a> |
			</div>

		</div>

	</div>

	<div class="newsticker">
		<strong>Senaste nytt:</strong>
		<span class="news_datetime">-</span>
		<a href="<?= $urlMain ?>" class="news_link"><?= $noNewsText ?></a>
	</div>

</div>

</div>
