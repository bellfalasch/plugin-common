<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://guide.ffuniverse.nu/
 * @since      1.0.0
 *
 * @package    Ffuniverse_Nu
 * @subpackage Ffuniverse_Nu/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<div class="wrap">

    <h2><?php echo esc_html(get_admin_page_title()); ?></h2>

    <form method="post" name="cleanup_options" action="options.php">

    <?php
        //Grab all options
        $options = get_option($this->plugin_name);

        // Cleanup
        $topnav5 = isset($options['tn5']) ? $options['tn5'] : '1';
        $endnav = isset($options['endnav']) ? $options['endnav'] : '2';
        $cleanup = isset($options['cleanup']) ? $options['cleanup'] : '1';
        $comments_css_cleanup = isset($options['comments_css_cleanup']) ? $options['comments_css_cleanup'] : '1';
        $gallery_css_cleanup = isset($options['gallery_css_cleanup']) ? $options['gallery_css_cleanup'] : '';

        // New Login customization vars
        $login_logo_id = isset($options['login_logo_id']) ? $options['login_logo_id'] : '';
        $login_logo = wp_get_attachment_image_src( $login_logo_id, 'thumbnail' );
        $login_logo_url = isset($login_logo[0]) ? $login_logo[0] : '';
    ?>

    <?php
        settings_fields($this->plugin_name);
        do_settings_sections($this->plugin_name);
    ?>

    <h3 class="nav-tab-wrapper">TopNav</h2>
    <p>TopNav can only attach to themes that implement the hook "ffu_topnav" just after the opening of the body-tag.</p>

    <fieldset>
        <label for="<?php echo $this->plugin_name; ?>-topnav-1">
            <input type="checkbox" id="<?php echo $this->plugin_name; ?>-topnav-1" name="<?php echo $this->plugin_name; ?>[tn5]" value="1" <?php checked($topnav5, 1); ?> />
            <span><strong>Modern</strong> (TopNav 5 and the "Go to top"-button)</span><br />
        </label>
        <label for="<?php echo $this->plugin_name; ?>-topnav-2">
            <input type="checkbox" id="<?php echo $this->plugin_name; ?>-topnav-2" name="<?php echo $this->plugin_name; ?>[tn5]" value="2" <?php checked($topnav5, 2); ?> />
            <span><strong>Legacy</strong> (displays TopNav 3)</span><br />
        </label>
        <label for="<?php echo $this->plugin_name; ?>-topnav-0">
            <input type="checkbox" id="<?php echo $this->plugin_name; ?>-topnav-0" name="<?php echo $this->plugin_name; ?>[tn5]" value="0" <?php checked($topnav5, 0); ?> />
            <span>None</span><br />
        </label>
    </fieldset>
    <br />

    <h3 class="nav-tab-wrapper">EndNav</h2>
    <p>EndNav can only attach to themes that implement the hook "ffu_endnav" just before the closing body-tag.</p>
    <fieldset>
        <label for="<?php echo $this->plugin_name; ?>-endnav-2">
            <input type="radio" id="<?php echo $this->plugin_name; ?>-endnav-2" name="<?php echo $this->plugin_name; ?>[endnav]" value="2" <?php checked($endnav, 2); ?> />
            <span><strong>Full</strong> (works best within our guides)</span><br />
        </label>
        <label for="<?php echo $this->plugin_name; ?>-endnav-1">
            <input type="radio" id="<?php echo $this->plugin_name; ?>-endnav-1" name="<?php echo $this->plugin_name; ?>[endnav]" value="1" <?php checked($endnav, 1); ?> />
            <span><strong>Light</strong> (works best on our none-guide sites)</span><br />
        </label>
        <label for="<?php echo $this->plugin_name; ?>-endnav-0">
            <input type="radio" id="<?php echo $this->plugin_name; ?>-endnav-0" name="<?php echo $this->plugin_name; ?>[endnav]" value="0" <?php checked($endnav, 0); ?> />
            <span>None</span><br />
        </label>
    </fieldset>
    <br />

    <h3 class="nav-tab-wrapper">Cleanups</h2>
    <p>Helps keeping the html clean, and the download size of our pages at a minimum.</p>

    <!-- remove some meta and generators from the <head> -->
    <fieldset>
        <legend class="screen-reader-text">
            <span>Clean WordPress head section</span>
        </legend>
        <label for="<?php echo $this->plugin_name; ?>-cleanup">
            <input type="checkbox" id="<?php echo $this->plugin_name; ?>-cleanup" name="<?php echo $this->plugin_name; ?>[cleanup]" value="1" <?php checked($cleanup, 1); ?> />
            <span><?php esc_attr_e('Clean up the head section', $this->plugin_name); ?></span>
        </label>
    </fieldset>

    <!-- remove injected CSS from comments widgets -->
    <fieldset>
        <legend class="screen-reader-text"><span>Remove Injected CSS for comment widget</span></legend>
        <label for="<?php echo $this->plugin_name; ?>-comments_css_cleanup">
            <input type="checkbox" id="<?php echo $this->plugin_name; ?>-comments_css_cleanup" name="<?php echo $this->plugin_name; ?>[comments_css_cleanup]" value="1" <?php checked($comments_css_cleanup, 1); ?> />
            <span><?php esc_attr_e('Remove Injected CSS for comment widget', $this->plugin_name); ?></span>
        </label>
    </fieldset>

    <!-- remove injected CSS from gallery -->
    <fieldset>
        <legend class="screen-reader-text"><span>Remove Injected CSS for galleries</span></legend>
        <label for="<?php echo $this->plugin_name; ?>-gallery_css_cleanup">
            <input type="checkbox" id="<?php echo $this->plugin_name; ?>-gallery_css_cleanup" name="<?php echo $this->plugin_name; ?>[gallery_css_cleanup]" value="1" <?php checked( $gallery_css_cleanup, 1 ); ?>  />
            <span><?php esc_attr_e('Remove Injected CSS for galleries', $this->plugin_name); ?></span>
        </label>
    </fieldset>
    <br />

    <!-- Login page customizations -->
    <h2 class="nav-tab-wrapper">Login customization</h2>
    <p>Add logo to login form change buttons and background color</p>

    <!-- add your logo to login -->
    <fieldset>
        <legend class="screen-reader-text"><span><?php esc_attr_e('Login Logo', $this->plugin_name);?></span></legend>
        <label for="<?php echo $this->plugin_name;?>-login_logo">
            <input type="hidden" id="login_logo_id" name="<?php echo $this->plugin_name;?>[login_logo_id]" value="<?php echo $login_logo_id; ?>" />
            <input id="upload_login_logo_button" type="button" class="button" value="<?php _e( 'Upload Logo', $this->plugin_name); ?>" />
            <span><?php esc_attr_e('Login Logo', $this->plugin_name);?></span>
        </label>
        <div id="upload_logo_preview" class="wp_cbf-upload-preview <?php if(empty($login_logo_id)) echo 'hidden'?>">
            <img src="<?php echo $login_logo_url; ?>" />
            <button id="<?php echo $this->plugin_name;?>-delete_logo_button" class="<?php echo $this->plugin_name;?>-delete-image">X</button>
        </div>
    </fieldset>

    <?php submit_button('Save all changes', 'primary','submit', TRUE); ?>


    </form>

</div>
